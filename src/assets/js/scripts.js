/*!
 * fastshell
 * Fiercely quick and opinionated front-ends
 * https://HosseinKarami.github.io/fastshell
 * @author Hossein Karami
 * @version 1.0.5
 * Copyright 2018. MIT licensed.
 */
(function ($, window, document, undefined) {

  'use strict';

  $(function () {
    // FastShell
  });

  $('a[href*="#"]:not([href="#"], [href="#myCarousel"])')
    .click(function() {
      if (location.pathname.replace(/^\//, '') ===
      this.pathname.replace(/^\//, '') &&
       location.hostname === this.hostname) {
        var target = $(this.hash);
        target = target.length ? target :
        $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });

    $(window).on('scroll touchmove', function() {
  $('.header').toggleClass('shrink', $(document).scrollTop() > 0);
});

$('#menu-toggle').click(function(){
    $('#menu').toggle();
});

// init Isotope
var $grid = $('.grid').isotope({
  itemSelector: '.content-card',
  layoutMode: 'fitRows',
    fitRows: {
      gutter: 20
    }
});
// layout Isotope after each image loads
$grid.imagesLoaded().progress( function() {
  $grid.isotope('layout');
});

$('.filter-button-group').on( 'click', 'li', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});

})(jQuery, window, document);
