import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";
import {Title} from "@angular/platform-browser";

import { SystemconfigService } from '../systemconfig.service';


declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-paper',
  templateUrl: './paper.component.html',
  styleUrls: ['./paper.component.scss']
})
export class PaperComponent implements OnInit {

  /* posts$: Object; */
  posts = <any>Object;
  path = 'news-room';
  current_page = 0;
  isocheck = false;
  pages = '';

  config = <any>Object;

  systemConfigService: SystemconfigService;

  constructor(private wpApiPosts: WpApiPosts, private route: ActivatedRoute, private router: Router, private titleService:Title, systemConfigService: SystemconfigService) {
    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
  }

  getPosts() {
    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts/'+this.current_page+'?_embed'})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
      this.titleService.setTitle("Vox Content Hub | "+this.posts.title.rendered);
    });
  }



  ngOnInit() {

    this.route
      .queryParams
      .subscribe(params => {
        this.current_page = +params['page'] || 0;

          this.getPosts();
          $(window).scrollTop(0);
      });

    this.getPosts();

    setTimeout(function(){
        $('.preloader').hide();
    }, 2000);
  }

}
