import { Component, OnInit, Input, EventEmitter} from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from "@angular/platform-browser";

import { SystemconfigService } from '../systemconfig.service';
import { MetaConfig, MetaService } from 'ng2-meta';

declare var jQuery:any;
declare var $:any;
declare var Isotope: any;

@Component({
  selector: 'app-case-studies',
  templateUrl: './case-studies.component.html',
  styleUrls: ['./case-studies.component.scss']
})

export class CaseStudiesComponent implements OnInit {

  /* posts$: Object; */
  posts = [];
  isocheck = false;
  pages = '';
  current_page = 1;
  userEdit = false;
  path = 'case-studies';
  cat_id = 95;
  parent_id = 95;
  categories = [];
  config = <any>Object;

  systemConfigService: SystemconfigService;


  constructor(private wpApiPosts: WpApiPosts, private route: ActivatedRoute, private router: Router, private titleService:Title, systemConfigService: SystemconfigService, private metaService: MetaService) {
    this.titleService.setTitle("Vox Content Hub | Case Studies");
    this.metaService.setTag('og:image','https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg');
    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
    this.cat_id = this.config.case_category;
    this.parent_id = this.config.case_category;
  }

  getPosts() {

       /* /wp/v2/categories?slug=some-category slug */
      /* /wp/v2/posts?categories=some-category-id */

    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=6&page='+this.current_page+'&categories='+this.cat_id})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
      this.isocheck = true;
      this.pages = response.headers.get('X-WP-TotalPages');
    });
  }


  cardLoaderCompleted()
  {
    var objects = this.categories;

    $( ".category_finder" ).each(function() {

      var p = $( this );
      var ids_text = $( this ).text();
      var IDs = ids_text.split(",");
      var category = '';

      IDs.forEach(function(element) {

        var id = element;
          objects.forEach(element => {
              if(id == element.id) {
                p.text(element.name);
                category = element.name;
              }
          });
      });


    });


    $('.preloader').hide();
  }

  getCategories()
  {
    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/categories?parent='+this.parent_id})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.categories = json;
    });
  }

  changeCategory(cat_id : any)
  {
    this.current_page = 1;
    var search_url = this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=6&page='+this.current_page+'&categories='+this.cat_id;

    if(cat_id != '' && cat_id != null && cat_id != 'all') {
      search_url = this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=6&page='+this.current_page+'&categories='+cat_id;
      this.cat_id = cat_id;
    }


    this.wpApiPosts.getList({url:search_url})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
      this.isocheck = true;
      this.pages = response.headers.get('X-WP-TotalPages');
    });

  }

  nextPage() {
    this.router.navigate(['case-studies'], { queryParams: { page: this.current_page + 1 } });
  }

  runIsotope()
  {
      this.posts = [];
  }


  ngOnInit() {

  this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.current_page = +params['page'] || 1;

        if(this.current_page < 1)
           this.current_page = 1;

        this.getPosts();
    this.getCategories();
    $(window).scrollTop(0);

      });

  this.getPosts();
    this.getCategories();

  }



}
