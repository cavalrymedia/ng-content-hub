import { Component, OnInit, Input } from '@angular/core';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.scss']
})
export class ShareComponent implements OnInit {

  constructor() { }

  @Input() i : any;

  status : any;

  sharer(t:any) {
    this.status = t;
    if ($( "#" + t ).hasClass( "d-block" )) {
      $( "#" + t  ).removeClass( "d-block" );
      $( "#" + t  ).addClass( "d-none" );
    } else {
      $( "#" + t  ).removeClass( "d-none" );
      $( "#" + t  ).addClass( "d-block" );
    }
  }

  ngOnInit() {
  }

}
