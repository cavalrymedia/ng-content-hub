import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {
  WpApiModule,
  WpApiLoader,
  WpApiStaticLoader
} from 'wp-api-angular';
import { DisqusModule } from "ngx-disqus";
import { MetaModule, MetaConfig, MetaService } from 'ng2-meta';

import { SimpleSmoothScrollModule } from 'ng2-simple-smooth-scroll';
import { ShareButtonsModule } from '@ngx-share/buttons';
import { ExcerptFilter } from './pipes/excerpt.pipe';
import { SystemconfigService } from './systemconfig.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PostsComponent } from './posts/posts.component';
import { HomeComponent } from './home/home.component';
import { WhitePapersComponent } from './white-papers/white-papers.component';
import { NewsRoomComponent } from './news-room/news-room.component';
import { CaseStudiesComponent } from './case-studies/case-studies.component';
import { ThanksComponent } from './thanks/thanks.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { CardComponent } from './card/card.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DisqusComponent } from './disqus/disqus.component';
import { SearchComponent } from './search/search.component';
import { FilterPipe } from './filter.pipe';
import { ArticleComponent } from './article/article.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { PaperComponent } from './paper/paper.component';
import { PreloaderComponent } from './preloader/preloader.component';
import { CaseSliderComponent } from './case-slider/case-slider.component';
import { PaperSliderComponent } from './paper-slider/paper-slider.component';
import { NewsSliderComponent } from './news-slider/news-slider.component';
import { DownloadComponent } from './download/download.component';
import { ShareComponent } from './share/share.component';

@NgModule({
  declarations: [
    AppComponent,
    PostsComponent,
    HomeComponent,
    WhitePapersComponent,
    NewsRoomComponent,
    CaseStudiesComponent,
    ThanksComponent,
    FooterComponent,
    HeaderComponent,
    CardComponent,
    PaginationComponent,
    SearchComponent,
    FilterPipe,
    ArticleComponent,
    DisqusComponent,
    SidebarComponent,
    PaperComponent,
    PreloaderComponent,
    PaperComponent,
    CaseSliderComponent,
    ExcerptFilter,
    PaperSliderComponent,
    NewsSliderComponent,
    DownloadComponent,
    ShareComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    WpApiModule.forRoot({ // <---
      provide: WpApiLoader,
      useFactory: (WpApiLoaderFactory),
      deps: [Http]
    }),
    DisqusModule.forRoot('vox-telecom'),
    ShareButtonsModule.forRoot(),
    SimpleSmoothScrollModule,
    MetaModule.forRoot(),
  ],
  providers: [SystemconfigService],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function WpApiLoaderFactory(http: Http) {
  return new WpApiStaticLoader(http, 'https://vox.co.za/wp-json/wp/v2/', '');
}
