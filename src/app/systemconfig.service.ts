import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SystemconfigService {

private contenthub_env_config = {
     
     url: 'http://commander.cavalryapps.com/ci-vox-master/staging/public_html' ,
     case_category: 95,
     news_category: 6,
     whitepaper_category: 93,
     publicKey : '1af6e8105e',
	 privateKey : 'e68f75749150462',
	 downloadFormId : '5'

  };

  constructor() { }

  	getEnvConfig() {
    	return this.contenthub_env_config;
  	}
}
