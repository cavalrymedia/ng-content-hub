import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  /* posts$: Object; */
  posts = [];

  /* constructor(private data: DataService) { } */
  constructor(private wpApiPosts: WpApiPosts) {
    this.getPosts();
  }
  getPosts() {
    this.wpApiPosts.getList()
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });
  }

  ngOnInit() {
  }

}
