import { Component, OnInit } from '@angular/core';

declare var jQuery:any;
declare var $:any;

import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';

import { ActivatedRoute, Router } from '@angular/router';
import { SystemconfigService } from '../systemconfig.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  posts = [];
  cat = 1;
  current_page = 0;
  article = [];
  config = <any>Object;

  systemConfigService: SystemconfigService;

  constructor(private wpApiPosts: WpApiPosts, private route: ActivatedRoute, private router: Router, systemConfigService: SystemconfigService) {
    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
  }

  getArticle() {

   	this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts/'+this.current_page+'?_embed'})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.article = json;
     this.cat = json.categories;
      this.getPosts();
    });

  }

  getPosts() {

  	this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=4&categories='+this.cat})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });


  }

  ngOnInit() {
  	this.route.queryParams
      .subscribe(params => {
        this.current_page = params.page || 0;

        this.getArticle();
        $(window).scrollTop(0);
      });

    this.getArticle();


  }

}
