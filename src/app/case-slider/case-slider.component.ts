import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';

import { SystemconfigService } from '../systemconfig.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-case-slider',
  templateUrl: './case-slider.component.html',
  styleUrls: ['./case-slider.component.scss']
})
export class CaseSliderComponent implements OnInit {

  /* posts$: Object; */
  posts = [];
  config = <any>Object;
  cat_id = 95;

  trigger = false;

  systemConfigService: SystemconfigService;

  constructor(private wpApiPosts: WpApiPosts, systemConfigService: SystemconfigService) {

    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
    this.cat_id = this.config.case_category;
  }

  getPosts() {
    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=3&categories='+this.cat_id})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });
  }

  ngOnInit() {
    this.getPosts();
  }

  cardLoaderCompleted() {

    if(this.trigger === false) {
      var articles = $('#studies').find('.carousel-indicators > li');
      var articleLength = articles.length - 1;
      var articleCarousel = $('#studies');
      $.each(articles,function(i,ele){
        $(ele).attr('data-slide-to',i).on('click',function(e){

          $(articleCarousel).carousel(i);

        });
      });
    }

    this.trigger = true;

  }

}
