import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { WhitePapersComponent } from './white-papers/white-papers.component';
import { NewsRoomComponent } from './news-room/news-room.component';
import { CaseStudiesComponent } from './case-studies/case-studies.component';
import { ThanksComponent } from './thanks/thanks.component';
import { SearchComponent } from './search/search.component';
import { ArticleComponent } from './article/article.component';
import { PaperComponent } from './paper/paper.component';

import { MetaGuard } from 'ng2-meta';

const routes: Routes = [
  { path: '', component: HomeComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Home',
        description: 'Home page description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'white-papers', component: WhitePapersComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'White Papers',
        description: 'White papers description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'white-papers/:slug', component: PaperComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'White Papers',
        description: 'White papers description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'news-room', component: NewsRoomComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'News Room',
        description: 'News Room description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'news-room/:slug', component: ArticleComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'News Room',
        description: 'News Room description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'case-studies', component: CaseStudiesComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Case Studies',
        description: 'Case Studies description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'case-studies/:slug', component: ArticleComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Case Studies',
        description: 'Case Studies description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'thanks', component: ThanksComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Thank You',
        description: 'Thank You description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'posts', component: PostsComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Posts',
        description: 'Posts description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'article', component: ArticleComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Article',
        description: 'Article description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'search', component: SearchComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Search',
        description: 'Search description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },
  { path: 'paper', component: PaperComponent,
  canActivate: [MetaGuard],
    data: {
      meta: {
        title: 'Paper',
        description: 'Paper description',
        'og:image': 'https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg'
      }
    }
  },

  { path: '**', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
