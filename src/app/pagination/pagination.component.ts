import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input() pages: any;
  @Input() current_page: any;
  @Input() search: any;
  @Input() path: any;

  constructor() { }

  ngOnInit() {
  }

}
