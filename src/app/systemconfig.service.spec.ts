import { TestBed, inject } from '@angular/core/testing';

import { SystemconfigService } from './systemconfig.service';

describe('SystemconfigService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SystemconfigService]
    });
  });

  it('should be created', inject([SystemconfigService], (service: SystemconfigService) => {
    expect(service).toBeTruthy();
  }));
});
