import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router) { }

  search_text = '';

  ngOnInit() {
    $(window).on('scroll touchmove', function() {
      $('.header').toggleClass('shrink', $(document).scrollTop() > 0);
    });
    $("#menu-toggle").on("click", function (event) {
        $('#menu').toggle();
        ($('#menu-toggle').hasClass('opened') == true ? $('#menu-toggle').toggleClass('opened',false) : $('#menu-toggle').toggleClass('opened',true))
        event.stopPropagation();
    });
    $("#menu").on("click", function (event) {
        event.stopPropagation();
    });
    $("body").on("click", function () {
      $('#menu').hide();
      $('#menu-toggle').toggleClass('opened',false);
    });
  }

   goToSearch()
    {

    var search_term = $('.search').val();
      if($('.search').val() == '' || $('.search').val() == null)
        return false;

      else {
        this.router.navigate(['search'], { queryParams: { s: search_term} });
       // window.location.href = '/search?s='+;
      }
    }


}
