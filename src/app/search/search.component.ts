import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import {Title} from "@angular/platform-browser";

import { SystemconfigService } from '../systemconfig.service';

declare var jQuery:any;
declare var $:any;
declare var Isotope: any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  posts = [];
  isocheck = false;
  pages = '';
  current_page = 1;
  userEdit = false;
  search = '';
  search_text = '';
  path = 'search';
  config = <any>Object;


  systemConfigService: SystemconfigService;


  constructor(private wpApiPosts: WpApiPosts, private route: ActivatedRoute, private router: Router, private titleService:Title, systemConfigService: SystemconfigService) {

      this.titleService.setTitle("Vox Content Hub | Search");
      this.systemConfigService = systemConfigService;
      this.config = this.systemConfigService.getEnvConfig();
  }

  getPosts() {



    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=6&search='+this.search+'&page='+this.current_page})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
      this.isocheck = true;
      this.pages = response.headers.get('X-WP-TotalPages');
    });
  }

  nextPage() {
    this.router.navigate(['case-studies'], { queryParams: { page: this.current_page + 1 } });
  }

  runIsotope()
  {


  }


  ngOnInit() {

  this.route.queryParams
      .subscribe(params => {
        this.search = params.s || '';
        this.search_text = this.search;
        //this.search = encodeURIComponent(this.search);
        this.current_page = params.page || 1;

        if(this.current_page < 1)
           this.current_page = 1;

         this.getPosts();
         $(window).scrollTop(0);

      });


    this.getPosts();

    setTimeout(function(){
        $('.preloader').hide();
    }, 800);

  }

}
