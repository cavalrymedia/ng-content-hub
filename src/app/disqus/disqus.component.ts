import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-disqus',
  styleUrls: ['./disqus.component.scss'],
  template: `<disqus [identifier]="article"></disqus>`
})
export class DisqusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
