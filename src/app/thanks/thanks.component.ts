import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss']
})
export class ThanksComponent implements OnInit {

  current_page = 0;
  slug = '';

  constructor(private titleService:Title, private route: ActivatedRoute, private router: Router) {
  	this.titleService.setTitle("Vox Content Hub | Thank You");
  }

  ngOnInit() {
  		$(window).scrollTop(0);
      this.route
        .queryParams
        .subscribe(params => {
          this.current_page = +params['page'] || 0;
          this.slug = params['slug'] || '';

        });
  }
}
