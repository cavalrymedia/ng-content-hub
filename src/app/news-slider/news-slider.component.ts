import { Component, OnInit } from '@angular/core';

import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';

import { SystemconfigService } from '../systemconfig.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-news-slider',
  templateUrl: './news-slider.component.html',
  styleUrls: ['./news-slider.component.scss']
})
export class NewsSliderComponent implements OnInit {

  /* posts$: Object; */
  posts = [];
  config = <any>Object;
  cat_id = 6;

  systemConfigService: SystemconfigService;
  newsRoomFired = false;

  constructor(private wpApiPosts: WpApiPosts, systemConfigService: SystemconfigService) {
    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
    this.cat_id = this.config.news_category;
  }

  getPosts() {
    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=3&categories='+this.cat_id})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });
  }

  setNewsArticleHeights() {

    if(this.newsRoomFired === false) {

      var artiMaxHeight = 0;
      var articles = $('.news-article');

      $.each(articles,function(i,e){

        var contentH = $(this).find('.news-content').height();
        if(contentH > artiMaxHeight) {
          artiMaxHeight = contentH;
        }

        var imageUrl = $(this).find('.news-image').attr('src');
        $(this).find('.news-image').hide();
        $(this).find('.news-image-container').attr('style','background-image:url("' + imageUrl + '");');

      });

      articles.find('> div').height(artiMaxHeight);

    }

    this.newsRoomFired = true;
  }

  ngOnInit() {
    this.getPosts();
  }

}
