import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';
import {Title} from "@angular/platform-browser";
import { MetaService } from 'ng2-meta';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  /* posts$: Object; */
  posts = [];
  case_category = 1788;
  white_category = 1779;

  constructor(private metaService: MetaService, private wpApiPosts: WpApiPosts, private titleService:Title) {
    this.titleService.setTitle("Vox Content Hub | Home");
    this.metaService.setTag('og:image','https://www.vox.co.za/wp-content/uploads/2018/09/og-image.jpg');
    this.getPosts();
  }
  getPosts() {
    this.wpApiPosts.getList()
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });
  }

  ngOnInit() {

      setTimeout(function(){
        $('.preloader').hide();
    }, 1500);

  }

}
