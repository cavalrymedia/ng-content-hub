import { Component, OnInit, Input } from '@angular/core';

declare var jQuery:any;
declare var $:any;
declare var Isotope: any;

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  /* posts$: Object; */
  @Input() posts: any;

  /* constructor(private data: DataService) { } */
  constructor() {}


  ngOnInit() {
      setTimeout(function(){



        var $grid = $('.grid').isotope({
        itemSelector: '.content-card',
        layoutMode: 'fitRows',
          fitRows: {
            gutter: 20
          }
        });

        $grid.imagesLoaded().always( function() {
          $grid.isotope('layout');
        });

        $('.filter-button-group').on( 'click', 'li', function() {
          var filterValue = $(this).attr('data-filter');
          $grid.isotope({ filter: filterValue });
        });

      
    }, 3000);
  }

}
