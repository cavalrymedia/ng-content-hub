import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';

import { SystemconfigService } from '../systemconfig.service';

declare var jQuery:any;
declare var $:any;

@Component({
  selector: 'app-paper-slider',
  templateUrl: './paper-slider.component.html',
  styleUrls: ['./paper-slider.component.scss']
})
export class PaperSliderComponent implements OnInit {

  /* posts$: Object; */
  posts = <any>Object;
  config = <any>Object;
  cat_id = 6;

  systemConfigService: SystemconfigService;
  trigger = false;

  @Input() category: any;

  /* constructor(private data: DataService) { } */
  constructor(private wpApiPosts: WpApiPosts, systemConfigService: SystemconfigService) {

    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
    this.cat_id = this.config.whitepaper_category;
  }
  getPosts() {
    this.wpApiPosts.getList({url: this.config.url+'/wp-json/wp/v2/posts?_embed&per_page=3&categories='+this.cat_id})
    .toPromise()
    .then( response => {
      let json: any = response.json();
      this.posts = json;
    });
  }

  ngOnInit() {
      this.getPosts();
  }

  cardLoaderCompleted() {

    if(this.trigger === false) {
      var articles = $('#papers').find('.carousel-indicators > li');
      var articleLength = articles.length - 1;
      var articleCarousel = $('#papers');
      $.each(articles,function(i,ele){
        $(ele).attr('data-slide-to',i).on('click',function(e){

          $(articleCarousel).carousel(i);

        });
      });
    }

    this.trigger = true;

  }

}
