import { Component, OnInit, Input } from '@angular/core';

import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { WpApiPosts } from 'wp-api-angular';
import { Headers } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from "@angular/common/http";

import { SystemconfigService } from '../systemconfig.service';

declare var jQuery:any;
declare var $:any;
declare var CryptoJS:any;

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss']
})
export class DownloadComponent implements OnInit {

	@Input() download_link: any;
  @Input() slug: any;
  current_page = 0;
  config = <any>Object;

  systemConfigService: SystemconfigService;

  constructor(private httpClient:HttpClient, private route: ActivatedRoute, private router: Router, systemConfigService: SystemconfigService) {
    this.systemConfigService = systemConfigService;
    this.config = this.systemConfigService.getEnvConfig();
  }

   CalculateSig(stringToSign, privateKey){
        //calculate the signature needed for authentication
        var hash = CryptoJS.HmacSHA1(stringToSign, privateKey);
        var base64 = hash.toString(CryptoJS.enc.Base64);
        return encodeURIComponent(base64);
    }

  onSubmit(submittedForm) {

  	//set variables
    var d = new Date;
    var expiration = 3600; // 1 hour,
    var unixtime = d.getTime() / 1000;
    var future_unixtime = unixtime + expiration;
    var publicKey = this.config.publicKey;
    var privateKey = this.config.privateKey;
    var method = "POST";
    var route = "forms/'+this.config.downloadFormId+'/entries";

    var stringToSign = publicKey + ":" + method + ":" + route + ":" + future_unixtime;
    var sig = this.CalculateSig(stringToSign, privateKey);
    var url = this.config.url+'/gravityformsapi/' + route + '?api_key=' + publicKey + '&signature=' + sig + '&expires=' + future_unixtime;

    if (submittedForm.invalid) {
      return;
    }

    var entries =
	[
	    {
	    	1  : submittedForm.value.input_1,
	        2  : submittedForm.value.input_2,
	        3  : submittedForm.value.input_3,
	        4  : submittedForm.value.input_4
	    }
	];


    var values_json = JSON.stringify(entries);
    var current_slug = $('#slug').text();


    this.httpClient.post(url, values_json)
        .subscribe(
            data => {
                $('#download').get(0).click();
                this.router.navigate(['thanks'], { queryParams: { page: this.current_page, slug: current_slug } });
                //location.href = $('#download').prop('href');
				//return false;
            },
            error => {
            	$('#download').get(0).click();
                this.router.navigate(['thanks'], { queryParams: { page: this.current_page, slug: current_slug } });
            }
        );
  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.current_page = +params['page'] || 0;

      });
  }

}
